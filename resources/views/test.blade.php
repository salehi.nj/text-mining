@extends('layout.admin')

@section('content')
    <form action="{{route('test.run')}}" method="post">

        <div class="col-md-12">
            <!-- Horizontal Form -->
            @csrf
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">تست api </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">متن</label>

                            <div class="col-sm-10">
                                <textarea name="text" class="form-control" rows="3" placeholder="متن"></textarea>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <h3 class="box-title">
                            @if (\Session::has('text'))
                                <div class="alert alert-success">
                                    نتیجه
                                    <br>
                                    <br>
                                    {!! \Session::get('text') !!}
                                </div>
                            @endif

                        </h3>

                    </div>
                    <!-- /.box-footer -->

                </div>
            </div>
            <!-- /.box -->
            <!-- general form elements disabled -->
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">تنظیمات کلی</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div role="form">


                        <!-- radio -->
                        <div class="form-group">
                            @foreach(App\core\SahamApi::MAP as $url=>$map)
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="url" id="" value="{{$url}}"
                                               checked="">
                                        {{$url}}
                                    </label>
                                </div>
                            @endforeach


                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right">ورود</button>
                        </div>
                        <!-- /.box-footer -->

                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
    </form>
    </div>
@endsection