@extends('layout.admin')

@section('content')
    <form action="{{route('export')}}" method="post">

        <div class="col-md-12">
            <!-- Horizontal Form -->
            @csrf
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">خروجی اکسل </h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="form-horizontal">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">متن</label>

                            <div class="col-sm-10">
                                <textarea name="content" class="form-control" rows="3" placeholder="content LIKE '%وپارس%'"></textarea>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
            <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right">گرفتن خروجی</button>
                    </div>
                    <!-- /.box-footer -->
        </div>
    </form>
@endsection
