<?php

namespace App\Http\Controllers;

use App\Core\SahamApi;
use App\Exports\Tweets;
use App\HashTag;
use App\Jobs\TextMiningRequest;
use App\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function back;
use Maatwebsite\Excel\Facades\Excel;
use function view;

class SahamController extends Controller
{
    //
    public function index()
    {
        return view('dashboard');
    }

    public function test()
    {

        return view('test');
    }

    public function test2()
    {

        return view('test2');
    }

    public function download()
    {
        return view('download');
    }

    public function runtest(Request $request)
    {

        $test = new SahamApi();
        $test->setHeaders();
        $test->setUrl($request->url);
        $test->setParams(['text' => $request->text]);
        $response = $test->send();
//        dd($text);
        return back()->with('text', $response);
    }

    public function runtest2(Request $request)
    {
        $test = new SahamApi();
        $test->setHeaders();
        $test->setParams(['text' => $request->text]);

        foreach ($request->url as $url) {
            $test->setUrl($url);
            $response = $test->send();
            $test->setParams(['text' => $response]);

        }

        return back()->with('text', $response);
    }

    public function runDashboard(Request $request)
    {
        $records = DB::table('sahams')->whereRaw($request->input('content'))->whereNull('sentiment_classifier')->get();
//        dd($records->count());
        $urls = $request->url;
        foreach ($records as $record) {
            dispatch(new TextMiningRequest($record, $urls));
        }
        return back();
    }

    public function countHashtags()
    {

        DB::table('hash_tags')
          ->orderBy('name')
          ->chunk(100, function ($hastags) {
              foreach ($hastags as $hashtag) {

                  $tags_cont = DB::table('sahams')->where('content', 'LIKE', '%#' . $hashtag->name . '%')->count();

                  $hashTag = HashTag::firstOrNew(['name' => $hashtag->name]);
                  $hashTag->count = $tags_cont;

                  $hashTag->save();
              }
          });

        print 'done!!';

    }

      public function export(Request $request)

    {
        return Excel::download(new Tweets($request), 'tweets.xlsx');

    }

    public function excel()
    {
        return view ('excel');
    }

    public function makeJalaliate()
    {
        Tweet::whereNull('jalali_sendTime')->chunk(800, function ($tweets) {
            foreach ($tweets as $tweet) {
                //
                $tweet->update([
                                  'jalali_sendTime' => verta($tweet->sendTime)->format('Y m d')
                              ]);
            }
        });

    }
}
