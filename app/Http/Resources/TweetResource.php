<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TweetResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
             'item_id'=>$this->item_id,
            'sendTime'=>$this->sendTime,
            'jalali_sendTime'=>$this->jalali_sendTime,
            'senderUsername'=>$this->senderUsername,
            'content'=>$this->content,
            'likeCount'=>$this->likeCount,
            'parentSenderName'=>$this->parentSenderName,
            'parentContent'=>$this->parentContent,
            'parentSendTime'=>$this->parentSendTime
        ];
    }
}
