<?php
/**
 * Created by PhpStorm.
 * User: Salehi
 * Date: 12/24/2019
 * Time: 8:35 AM
 */

namespace App\Core;


use Illuminate\Support\Facades\Cache;
use function dd;

class SahamApi
{
    public const BASE_URL = 'https://api.text-mining.ir/api/';

    private $token;
    private $url;
    private $body;
    private $params;
    private $client;
    private $is_crazy;
    public const MAP = [
        'PreProcessing/NormalizePersianWord'    => 'normal',
        'TextRefinement/FormalConverter'        => 'crazy',
        'TextRefinement/SwearWordTagger'        => 'crazy',
        'PreProcessing/SentenceSplitter'        => 'normal',
        'InformationRetrieval/StopWordRemoval'  => 'crazy',
        'TextRefinement/SpellCorrector'         => 'normal',
        'PosTagger/GetPos'                      => 'crazy',
        'SentimentAnalyzer/SentimentClassifier' => 'crazy',

    ];

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
//        Cache::forget('token');
        $token = Cache::get('token');
        $this->token = $token ?? $this->getToken();
    }

    public function send()
    {

        $this->body = $this->is_crazy ? $this->body['text'] : $this->body;
        $requesturi = $this->client->post($this->url, [
            'verify'  => false,
            'headers' => $this->params,
            'charset' => 'utf-8',
            'body'    => json_encode($this->body, JSON_UNESCAPED_UNICODE)
        ]);

        $result = $requesturi->getBody()->getContents();
        return $result;
    }

    public function setUrl($url)
    {

        $this->is_crazy = self::MAP[$url] === 'normal' ? false : true;

        $this->url = $this::BASE_URL . $url;
    }

    public function setParams(Array $params)
    {
        $this->body = $params ?? [];
    }

    public function setHeaders(Array $params = null)
    {
        $this->params = $params ?? [
                'cache-control' => 'no-cache',
                'Content-Type'  => 'application/json',
                'Authorization' => 'Bearer ' . $this->token,
            ];

    }

    public function getToken()
    {
        if (Cache::has('teken')) {
            return Cache::get('token');
        }
        $url = 'https://api.text-mining.ir/api/Token/GetToken?apikey='.env('API_KEY', false);

        $client = new \GuzzleHttp\Client();
        $requesturi = $client->get($url, [
            'verify'  => false,
            'headers' =>
                [
                    'Cache-Control' => 'no-cache',
                    'Content-Type'  => ' pplication/json',
                ]
        ]);
        $result = $requesturi->getBody()->getContents();
        $result = json_decode($result, false);
        Cache::put('token', $result->token, 60);
        return $result->token;
    }

}