<?php

namespace App\Exports;

use App\Http\Resources\TweetResource;
use App\Tweet;
use function dd;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Tweets implements FromCollection, WithHeadings
{
    private $content;


    public function __construct(Request $request)
    {
        $this->content = $request->input('content');
    }

    public function collection()
    {
//        return Tweet::whereRaw($this->content);
           $tweets = Tweet::whereRaw($this->content)->get();
//           dd($this->content,$tweets);
        return TweetResource::collection($tweets);

    }


    public function headings(): array
    {
        return [
            'item_id',
            'زمان ارسال',
            'زمان ارسال',
            'نام کاربری',
            'متن پیام',
            'تعداد لایک',
            'پیام اصلی',
            'متن پیام اصلی',
            'زمان ارسال پیام اصلی'
        ];
    }






}
