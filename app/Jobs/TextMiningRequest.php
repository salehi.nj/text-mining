<?php

namespace App\Jobs;

use App\Core\SahamApi;
use function dd;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class TextMiningRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $urls;
    private $record;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($record, Array $urls)
    {
        //
        $this->record = $record;
        $this->urls = $urls;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Redis::throttle('key')->allow(1)->every(4)->then(function () {
            // Job logic...
            $test = new SahamApi();
            $test->setHeaders();
            $test->setParams(['text' => $this->record->content]);

            foreach ($this->urls as $url) {
                $test->setUrl($url);
                $response = $test->send();
                $test->setParams(['text' => $response]);
            }
            DB::table('sahams')->whereId($this->record->id)->update([
                                                                        'sentiment_classifier' => $response??null,
                                                                    ]);
        }, function () {
            // Could not obtain lock...

            return $this->release(10);
        });
    }
}
