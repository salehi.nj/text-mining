<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    //
    protected $table = 'sahams';

    protected $fillable = [
        'item_id',
        'sendTime',
        'jalali_sendTime',
        'senderName',
        'senderUsername',
        'senderProfileImage',
        'content',
        'lastLikeNickName',
        'likeCount',
        'type',
        'imageUid',
        'mediaContentType',
        'scoredPostDate',
        'parentId',
        'parentSenderName',
        'parentSenderUsername',
        'parentContent',
        'parentSendTime',
        'raw'
    ];
}
