<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('dashboard');
})->name('home');

Route::post('test', 'SahamController@runtest')->name('test.run');
Route::post('test2', 'SahamController@runtest2')->name('test.run2');
Route::post('dashboard', 'SahamController@runDashboard')->name('dashboard.run');

Route::get('test', 'SahamController@test');
Route::get('test2', 'SahamController@test2');

Route::get('change-date', 'SahamController@makeJalaliate');

Route::get('export', 'SahamController@excel')->name('excel');
Route::post('export', 'SahamController@export')->name('export');

Route::get('download', function () {
    return view('download');
});
